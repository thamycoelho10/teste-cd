FROM node

LABEL maintainer="Thamy Coelho <thamycoelho10@gmail.com>"

WORKDIR /app

COPY ./app.js /app

RUN npm install express

ENTRYPOINT ["node", "app.js"]
